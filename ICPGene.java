// Implementation of ICP Problem
// Author: Sherri Goings
//
// This program is free software; you can redistribute it and/or 
// modify it under the terms of version 2 of the GNU General Public 
// License as published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
import java.util.*;
import java.awt.Point;
import java.io.*;
import gpjpp.*;

//extend GPGene to evaluate ICP

public class ICPGene extends GPGene {
    
    //public null constructor required during stream loading only
    public ICPGene() {}

    //this constructor called when new genes are created
    ICPGene(GPNode gpo) { super(gpo); }

    //this constructor called when genes are cloned during reproduction
    ICPGene(ICPGene gpo) { super(gpo); }

    //called when genes are cloned during reproduction
    protected Object clone() { return new ICPGene(this); }

    //ID routine required for streams
    public byte isA() { return GPObject.USERGENEID; }

    //must override GPGene.createChild to create ICPGene instances
    public GPGene createChild(GPNode gpo) { return new ICPGene(gpo); }
//requires the int currentRound in History.java
double evaluate(ICPVariables cfg, ICPGP gp) {
        Random myRandom = new Random();
        double arg1, arg2, arg3;

        double[] yourPast;
        double[] myPast;

        double result;
        double result1;
        double result2;
        double minimum;
        double maximum;
        double avg;

        int index;

        switch (node.value()) {

        case History.YOURLAST:
                return cfg.gameHistory.history2[cfg.gameHistory.currentRound];
        case History.YOURMIN:
                yourPast = cfg.gameHistory.history2;
                minimum = 1;
                for(int i = 0; i<yourPast.length; i++){
                        if (yourPast[i]<minimum){minimum = yourPast[i];}
                }
                return minimum;
        case History.YOURMAX:
            yourPast = cfg.gameHistory.history2;
                maximum = 0;
                for(int i = 0; i<yourPast.length; i++){
                        if (yourPast[i]>maximum){maximum = yourPast[i];}
                }
                return maximum;
        // case History.YOURAVG:
        //         yourPast = cfg.gameHistory.history1;
        //         avg = 0;
        //         for(int i = 0; i<yourPast.length; i++){
        //                 avg += yourPast[i];
        //         }
        //         return avg/yourPast.length;
        case History.MYLAST:
                return cfg.gameHistory.history1[cfg.gameHistory.currentRound];
        case History.MYMIN:
                myPast = cfg.gameHistory.history1;
                minimum = 1;
                for(int i = 0; i<myPast.length; i++){
                        if (myPast[i]<minimum){minimum = myPast[i];}
                }
                return minimum;
        case History.MYMAX:
            myPast = cfg.gameHistory.history1;
                maximum = 0;
                for(int i = 0; i<myPast.length; i++){
                        if (myPast[i]>maximum){maximum = myPast[i];}
                }
                return maximum;
        // case History.MYAVG:
        //     myPast = cfg.gameHistory.history2;
        //         avg = 0;
        //         for(int i = 0; i<myPast.length; i++){
        //                 avg += myPast[i];
        //         }
        //         return avg/myPast.length;

        case History.RAND:
            return myRandom.nextDouble();
        case History.EPS:
            return 0.05;
        case History.ONEMINEPS:
            return 0.95;
        case History.AVG:
            result = (( (ICPGene)get(0) ).evaluate(cfg, gp) + ( (ICPGene)get(1) ).evaluate(cfg, gp)) / 2;
            return result;
        case History.MULT:
            result = ( (ICPGene)get(0) ).evaluate(cfg, gp) * ( (ICPGene)get(1) ).evaluate(cfg, gp);
            return result;
        case History.ADD:
            result = ( (ICPGene)get(0) ).evaluate(cfg, gp) + ( (ICPGene)get(1) ).evaluate(cfg, gp);
            if (result>1) result = 1;
            return result;
        case History.SUB:
            result = ( (ICPGene)get(0) ).evaluate(cfg, gp) - ( (ICPGene)get(1) ).evaluate(cfg, gp);
            if (result<0) result = 0;
            return result;
        // case History.IFGT:
        //     result = ( (ICPGene)get(0) ).evaluate(cfg, gp) - ( (ICPGene)get(1) ).evaluate(cfg, gp);
        //     if (result>0) {
        //         return ( (ICPGene)get(2) ).evaluate(cfg, gp);
        //     } else {
        //         return ( (ICPGene)get(3) ).evaluate(cfg, gp);
        //     }

        // case History.AVGOFX:

        //     index = 5;
        //     result1 = ( (ICPGene)get(0) ).evaluate(cfg, gp);
        //     index = (int)(result1 * index);
        //     avg = 0;
        //     if (index >= cfg.gameHistory.numRounds) {
        //         index = cfg.gameHistory.numRounds - 1;
        //     }
        //     if (index == 0) {
        //         return 0;
        //     }
        //     myPast = cfg.gameHistory.history2;
        //     for(int i = 0; i<index; i++){
        //         avg += myPast[cfg.gameHistory.numRounds - i -1];
        //     }
        //     return avg/index;
            
        case History.MIN:
            result1 = ( (ICPGene)get(0) ).evaluate(cfg, gp);
            result2 = ( (ICPGene)get(1) ).evaluate(cfg, gp);
            if (result1 < result2) {
                return result1;
            }
            return result2;

        case History.MAX:
            result1 = ( (ICPGene)get(0) ).evaluate(cfg, gp);
            result2 = ( (ICPGene)get(1) ).evaluate(cfg, gp);
            if (result1 > result2) {
                return result1;
            }
            return result2;  

        // case History.EPSILON:
        //     return 0.06; 

        case History.XAGO:
            index = 5;
            result = ( (ICPGene)get(0) ).evaluate(cfg, gp);
            index = (int)(result * index);
            if (index >= cfg.gameHistory.numRounds) {
                index = cfg.gameHistory.numRounds - 1;
            }
            myPast = cfg.gameHistory.history2;
            avg = myPast[cfg.gameHistory.numRounds - index -1];
            return avg;

        case History.RAND50:
            if (myRandom.nextDouble() < .5) {
                return ( (ICPGene)get(0) ).evaluate(cfg, gp);
            } else {
                return ( (ICPGene)get(1) ).evaluate(cfg, gp);
            }

        case History.RAND90:
            if (myRandom.nextDouble() < .9) {
                return ( (ICPGene)get(0) ).evaluate(cfg, gp);
            } else {
                return ( (ICPGene)get(1) ).evaluate(cfg, gp);
            }


        default:
            throw new RuntimeException("Undefined function type "+node.value());
        }
    }

}
