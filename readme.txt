Members: Sam Vinitsky, Sam Spaeth, Sean Mullan, Mica Mantilla

External code: GPJPP
Modified code: Tartarus1 assignment (modified every file, but kept structure)

How to run code:
1. Navigate to the icp folder
2. run "javac *.class"
    2a. if that didn't work, run 'javac -cp "/path/to/folder/:." *.java' 
3. run "java ICP"
    3a. if that didn't work, run 'java -cp "/path/to/folder/:." ICP'
4. you can configure parameters in icp.ini
 
