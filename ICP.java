// gpjpp example program
// Copyright (c) 1997, Kim Kokkonen
//
// This program is free software; you can redistribute it and/or 
// modify it under the terms of version 2 of the GNU General Public 
// License as published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// Send comments, suggestions, problems to kimk@turbopower.com

import java.io.*;
import java.util.Properties;
import gpjpp.*;

//Lawnmower test
class ICP extends GPRun {

    //must override GPRun.createVariables to return lawn-specific variables
    protected GPVariables createVariables() { 
        return new ICPVariables(); 
    }

    //must override GPRun.createNodeSet to return 
    //  initialized set of functions & terminals
    protected GPAdfNodeSet createNodeSet(GPVariables cfg) {
        GPAdfNodeSet adfNs = new GPAdfNodeSet(1);
        GPNodeSet ns0 = new GPNodeSet(18);
        adfNs.put(0, ns0);

        // terminals
        ns0.putNode(new GPNode(History.YOURLAST, "yourLast"));
        ns0.putNode(new GPNode(History.YOURMIN, "yourMin"));
        ns0.putNode(new GPNode(History.YOURMAX, "yourMax"));
        //ns0.putNode(new GPNode(History.YOURAVG, "yourAvg"));
        ns0.putNode(new GPNode(History.MYLAST, "myLast"));
        ns0.putNode(new GPNode(History.MYMIN, "myMin"));
        ns0.putNode(new GPNode(History.MYMAX, "myMax"));
        //ns0.putNode(new GPNode(History.MYAVG, "myAvg"));
        ns0.putNode(new GPNode(History.RAND, "rand"));
        ns0.putNode(new GPNode(History.EPS, "epsilon"));
        ns0.putNode(new GPNode(History.ONEMINEPS, "1 - epsilon"));


        // functions
        ns0.putNode(new GPNode(History.AVG, "avg", 2));
        ns0.putNode(new GPNode(History.MULT, "mult", 2));
        ns0.putNode(new GPNode(History.ADD, "add", 2));
        ns0.putNode(new GPNode(History.SUB, "sub", 2));
        //ns0.putNode(new GPNode(History.IFGT, "if>", 4));
        //ns0.putNode(new GPNode(History.AVGOFX, "avgOfX", 1)); 
        ns0.putNode(new GPNode(History.MIN, "min", 2));
        ns0.putNode(new GPNode(History.MAX, "max", 2));
        //ns0.putNode(new GPNode(History.EPSILON, "epsilon"));
        ns0.putNode(new GPNode(History.XAGO, "xAgo", 1)); 
        ns0.putNode(new GPNode(History.RAND50, "rand50", 2)); 
        ns0.putNode(new GPNode(History.RAND90, "rand90", 2)); 


         
        return adfNs;
    }

    //must override GPRun.createPopulation to return 
    //  lawn-specific population
    protected GPPopulation createPopulation(GPVariables cfg, 
        GPAdfNodeSet adfNs) {
        return new ICPPopulation(cfg, adfNs);
    }

    //construct this test case
    ICP(String baseName) { super(baseName, true); }

    //main application function
    public static void main(String[] args) {

        //compute base file name from command line parameter
        String baseName;
        if (args.length == 1)
            baseName = args[0];
        else
            baseName = "icp";

        //construct the test case
        ICP test = new ICP(baseName);

        //run the test
        test.run();

        //make sure all threads are killed
        System.exit(0);
    }
}
