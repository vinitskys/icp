import java.util.*;
import java.io.*;
import java.lang.Math.*;

    
public class History {

    //functions and terminals
    public final static int YOURLAST = 0;
    public final static int YOURMIN = 1;
    public final static int YOURMAX = 2;
    //public final static int YOURAVG = 3;
    public final static int MYLAST = 3;
    public final static int MYMIN = 4;
    public final static int MYMAX = 5;
    //public final static int MYAVG = 7;
    public final static int RAND = 6;
    public final static int EPS = 7;
    public final static int ONEMINEPS = 8;

    public final static int AVG = 9;
    public final static int MULT = 10;
    public final static int ADD = 11;
    public final static int SUB = 12;
    //public final static int IFGT = 13;
    //public final static int AVGOFX = 14;
    public final static int MIN = 13;
    public final static int MAX = 14;

    //public final static int EPSILON = 19;
    public final static int XAGO = 15;
    public final static int RAND50 = 16;
    public final static int RAND90 = 17;



    public int numRounds;
    public int totalRounds;
    public int currentRound;
    public Random rgen;
    public double[] history1;
    public double[] history2;
    public double weight;

    public History(int totalRounds, double weight) {
        this.history1 = new double[totalRounds]; 
        this.history2 = new double[totalRounds]; 
        this.weight = weight;
        this.numRounds = 0;
        this.totalRounds = totalRounds;

        rgen = new Random();
        //initHistory();
    }

    public void addToHistory(double me, double you) {
        this.history1[numRounds] = me;
        this.history2[numRounds] = you;

        this.numRounds += 1;
    }

    // Low is better (returns 'number of years in prison')
    private double fitnessFunction(double me, double you, double weight) {
        return (Math.pow(me, weight) - 3*Math.pow(you,1/weight) + 3);

        // me:you
        // 1,1 : 1,1
        // 1,0 : 0,4
        // 0,0 : 3,3
    }

    // 1 = cooperate, 0 = defect
    // returns [fit1,fit2]
    public double[] calcFitness() {
        double fit1 = 0;
        double fit2 = 0;

        for (int i = 0; i < this.totalRounds; i++) {
            fit1 += fitnessFunction(this.history1[i], this.history2[i], this.weight);
            fit2 += fitnessFunction(this.history2[i], this.history1[i], this.weight);
        }
        double[] fitnesses = new double[2];
        fitnesses[0] = fit1;
        fitnesses[1] = fit2;
        return fitnesses;
    }

    public void print() {
        print(System.out);
    }
 
    // pointing in the correct direction
    public void print(PrintStream os) {
        os.print("testPrint");
    }

    public static void main(String args[]){

        double weight = 1.3;
        int totalRounds = 100;

        double totalFit1 = 0;
        double totalFit2 = 0;

        Random myRandom = new Random();

        for(int i=0;i<1000;i++){

            History thisHistory = new History(totalRounds, weight);

            for(int j=0;j<totalRounds;j++){

                double choice1 = 0;//myRandom.nextDouble();
                double choice2 = 0;

                if (j == 0) {
                    choice2 = 0;
                } 
                thisHistory.addToHistory(choice1, choice2);
            }

            double[] ourFit = thisHistory.calcFitness();
            totalFit1 += ourFit[0];
            totalFit2 += ourFit[1];

        }
        System.out.println("Rand v 0");
        System.out.println(totalFit1/1000.0);
        System.out.println(totalFit2/1000.0);
    }

}







